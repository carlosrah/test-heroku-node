process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/app');
var should = chai.should();
chai.use(chaiHttp);

it('Test web content', function(done) {
  chai.request(server)
  .get('/')
  .end(function(err, res){
    res.should.have.status(200);
    res.text.should.equal('Hello, World!!!!!!');
    done();
  });
});


